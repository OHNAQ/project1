import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAHRLUDR-vk8AxLux0MWJD7YpLEHdwCg24",
  authDomain: "project1-a2efc.firebaseapp.com",
  projectId: "project1-a2efc",
  storageBucket: "project1-a2efc.appspot.com",
  messagingSenderId: "250791322737",
  appId: "1:250791322737:web:47ec083e015a429f1e27d0",
  measurementId: "G-RCT3TN8B0V"
};


// Use this to initialize the firebase App
const firebaseApp = firebase.initializeApp(firebaseConfig);

// Use these for db & auth
const db = firebaseApp.firestore();
const auth = firebase.auth();

export { auth, db };


