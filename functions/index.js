const functions = require("firebase-functions");
const express = require('express')
const admin = require ('firebase-admin');


const app = express()
admin.initializeApp({
    credential: admin.credential.cert('./permissions.json'),
    databaseURL: "https://project1-a2efc-default-rtdb.firebaseio.com",
  });
const db = admin.firestore();
app.get("/hello-world",(req,res)=>{
    return res.status(200).json({message:'Hello world'})
});
app.post("/api/products",async(req,res)=>{
   await db.collection("products")
    .doc('/' + req.body.id +'/')
    .create({description:req.body.description});  
    return res.status(204).json();    
})
exports.app =functions.https.onRequest(app);
